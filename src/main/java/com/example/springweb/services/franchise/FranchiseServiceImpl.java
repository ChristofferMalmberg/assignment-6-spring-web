package com.example.springweb.services.franchise;

import com.example.springweb.exceptions.FranchiseNotFoundException;
import com.example.springweb.models.Franchise;
import com.example.springweb.repositories.FranchiseRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FranchiseServiceImpl implements FranchiseService {
    private final FranchiseRepository franchiseRepository;

    public FranchiseServiceImpl(FranchiseRepository franchiseRepository) {
        this.franchiseRepository = franchiseRepository;
    }

    //Finds a franchise by its id, throws not found exception.
    @Override
    public Franchise findById(Integer id) {
        return franchiseRepository.findById(id).orElseThrow(() -> new FranchiseNotFoundException(id));
    }

    //Finds all franchises in the database
    @Override
    public List<Franchise> findAll() {
        return franchiseRepository.findAll();
    }

    //Adds a franchise to the database
    @Override
    public Franchise add(Franchise entity) {
        return franchiseRepository.save(entity);
    }

    //Updates a franchise
    @Override
    public Franchise update(Franchise entity) {
        return franchiseRepository.save(entity);
    }

    //Sets a franchise field in its movies to null then deletes the franchise
    @Override
    public void deleteById(Integer id) {
        Franchise franchise = franchiseRepository.findById(id).orElseThrow(() -> new FranchiseNotFoundException(id));
        franchise.getMovies().forEach(movie -> movie.setFranchise(null));
        franchiseRepository.deleteById(id);
    }

    //Updates the movies belonging to the franchise
    @Override
    public void updateMovies(List<Integer> movies, int id) {
        movies.stream().forEach(x -> franchiseRepository.updateMovies(x, id));
    }

    //Checks if a franchise exists
    @Override
    public boolean franchiseExist(int id) {
        return franchiseRepository.existsById(id);
    }
}
