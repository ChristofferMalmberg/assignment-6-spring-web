package com.example.springweb.services.franchise;

import com.example.springweb.models.Franchise;
import com.example.springweb.services.CrudService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface FranchiseService extends CrudService<Franchise, Integer> {
    void updateMovies(List<Integer> movies, int id);

    boolean franchiseExist(int id);
}
