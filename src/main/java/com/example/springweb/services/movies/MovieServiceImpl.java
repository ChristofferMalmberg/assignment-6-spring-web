package com.example.springweb.services.movies;

import com.example.springweb.exceptions.MovieNotFoundException;
import com.example.springweb.models.Movie;
import com.example.springweb.repositories.MovieRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class MovieServiceImpl implements MovieService {

    private final MovieRepository movieRepository;

    public MovieServiceImpl(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

    //Finds a movie by its id, throws not found exception
    @Override
    public Movie findById(Integer id) {
        return movieRepository.findById(id).orElseThrow(() -> new MovieNotFoundException(id));
    }

    //Finds all movies in the database
    @Override
    public List<Movie> findAll() {
        return movieRepository.findAll();
    }

    //Adds a movie to the database
    @Override
    public Movie add(Movie entity) {
        return movieRepository.save(entity);
    }

    //Updates a movie
    @Override
    public Movie update(Movie entity) {
        return movieRepository.save(entity);
    }

    //Updates which characters belong to the movie
    @Override
    public void updateCharactersMovie(Set<Integer> characters, int movieId) {
        movieRepository.removeCharacters(movieId);
        characters.stream().forEach(characterId -> movieRepository.updateCharactersMovie(characterId, movieId));
    }

    //Deletes a movie from the linking table then deletes the movie from the database
    @Override
    public void deleteById(Integer id) {
        movieRepository.deleteMovieLinkingTable(id);
        movieRepository.deleteById(id);
    }

    //Checks if the movie exists
    @Override
    public boolean movieExists(int id) {
        return movieRepository.existsById(id);
    }
}

