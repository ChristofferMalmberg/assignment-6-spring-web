package com.example.springweb.services.movies;

import com.example.springweb.models.Movie;
import com.example.springweb.services.CrudService;

import java.util.Set;

public interface MovieService extends CrudService<Movie, Integer> {
    void updateCharactersMovie(Set<Integer> movies, int characterId);

    boolean movieExists(int id);
}
