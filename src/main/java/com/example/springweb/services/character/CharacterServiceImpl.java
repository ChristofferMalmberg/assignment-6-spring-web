package com.example.springweb.services.character;

import com.example.springweb.exceptions.CharacterNotFoundException;
import com.example.springweb.models.Character;
import com.example.springweb.repositories.CharacterRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CharacterServiceImpl implements CharacterService {
    private final CharacterRepository characterRepository;

    public CharacterServiceImpl(CharacterRepository characterRepository) {
        this.characterRepository = characterRepository;
    }

    //Finds a character by its id, throws notfound exception
    @Override
    public Character findById(Integer id) {
        return characterRepository.findById(id).orElseThrow(() -> new CharacterNotFoundException(id));
    }

    //Finds all the characters in the database
    @Override
    public List<Character> findAll() {
        return characterRepository.findAll();
    }

    //Adds a character to the database
    @Override
    public Character add(Character entity) {
        return characterRepository.save(entity);
    }

    //Updates a character
    @Override
    public Character update(Character entity) {
        return characterRepository.save(entity);
    }

    //Deletes a character by its id
    @Override
    public void deleteById(Integer id) {
        characterRepository.deleteById(id);
    }

    //Checks if a character exits
    @Override
    public boolean characterExists(int id) {
        return characterRepository.existsById(id);
    }
}
