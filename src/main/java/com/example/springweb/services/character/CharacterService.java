package com.example.springweb.services.character;

import com.example.springweb.models.Character;
import com.example.springweb.services.CrudService;
import org.springframework.stereotype.Service;

@Service
public interface CharacterService extends CrudService<Character, Integer> {
    boolean characterExists(int id);
}
