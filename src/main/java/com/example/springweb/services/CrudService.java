package com.example.springweb.services;

import java.util.List;

public interface CrudService<T, ID> {
    T findById(ID id);

    List<T> findAll();

    T add(T entity);

    T update(T entity);

    void deleteById(ID id);


}
