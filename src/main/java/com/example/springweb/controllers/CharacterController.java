package com.example.springweb.controllers;


import com.example.springweb.mappers.CharacterMapper;
import com.example.springweb.models.Character;
import com.example.springweb.models.dtos.CharacterDto;
import com.example.springweb.services.character.CharacterService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;


@RestController
@RequestMapping(path = "api/v1/characters")
public class CharacterController {

    private final CharacterService characterService;
    private final CharacterMapper characterMapper;

    public CharacterController(CharacterService characterService, CharacterMapper characterMapper) {
        this.characterService = characterService;
        this.characterMapper = characterMapper;
    }

    @Operation(summary = "Get all characters")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = CharacterDto.class)))})
    })
    //Gets all the characters in the database
    @GetMapping
    public ResponseEntity findAll() {
        Collection<CharacterDto> characterDtos = characterMapper.characterToCharacterDto(characterService.findAll());
        return ResponseEntity.ok(characterDtos);
    }

    @Operation(summary = "Get a character by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CharacterDto.class))}),
            @ApiResponse(responseCode = "404", content = {@Content(mediaType = "application/json")})
            //TODO Schema?
    })
    //Gets a character by id
    @GetMapping("{id}")
    public ResponseEntity findById(@PathVariable int id) {
        CharacterDto characterDto = characterMapper.characterToCharacterDto(characterService.findById(id));
        return ResponseEntity.ok(characterDto);
    }

    @Operation(summary = "Adds a new character")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description = "Character successfully added",
                    content = @Content)
    })
    //Adds a character to the database
    @PostMapping
    public ResponseEntity add(@RequestBody CharacterDto characterDto) {
        Character character = characterMapper.characterDtoTOCharacter(characterDto);
        character = characterService.add(character);
        URI uri = URI.create("character/" + character.getId());
        return ResponseEntity.created(uri).build();
    }

    @Operation(summary = "Updates a character")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Character successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Character could not be found by provided ID",
                    content = @Content)
    })
    //Updates a character
    @PutMapping("{id}")
    public ResponseEntity update(@RequestBody CharacterDto characterDto, @PathVariable int id) {
        if (characterDto.getId() != id) {
            return ResponseEntity.badRequest().build();
        }
        if (!characterService.characterExists(id)) {
            return ResponseEntity.notFound().build();
        }
        Character character = characterMapper.characterDtoTOCharacter(characterDto);
        characterService.update(character);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Deletes a character")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Character successfully deleted",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Character could not be found by provided ID",
                    content = @Content)
    })
    //Deletes a character
    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable int id) {
        if (!characterService.characterExists(id)) {
            return ResponseEntity.notFound().build();
        }
        characterService.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}
