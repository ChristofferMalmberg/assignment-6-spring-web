package com.example.springweb.controllers;

import com.example.springweb.mappers.FranchiseMapper;
import com.example.springweb.models.Franchise;
import com.example.springweb.models.dtos.FranchiseDto;
import com.example.springweb.services.franchise.FranchiseService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping(path = "/api/v1/franchises")
public class FranchiseController {

    private final FranchiseService franchiseService;
    private final FranchiseMapper franchiseMapper;

    public FranchiseController(FranchiseService franchiseService, FranchiseMapper franchiseMapper) {
        this.franchiseService = franchiseService;
        this.franchiseMapper = franchiseMapper;
    }

    @Operation(summary = "Get all franchises")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = FranchiseDto.class)))})
    })
    //Gets all Franchises
    @GetMapping
    public ResponseEntity findAll() {
        Collection<FranchiseDto> franchiseDtoList = franchiseMapper.franchiseToFranchiseDto(franchiseService.findAll());
        return ResponseEntity.ok(franchiseDtoList);
    }

    @Operation(summary = "Get a franchise by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = FranchiseDto.class))}),
            @ApiResponse(responseCode = "404", content = {@Content(mediaType = "application/json")})
            //TODO Schema?
    })
    //Gets a franchise by id
    @GetMapping("{id}")
    public ResponseEntity findById(@PathVariable int id) {
        FranchiseDto dto = franchiseMapper.franchiseToFranchiseDto(franchiseService.findById(id));
        return ResponseEntity.ok(dto);
    }

    @Operation(summary = "Adds a new franchise")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description = "Franchise successfully added",
                    content = @Content)
    })
    //Adds a franchise to the database
    @PostMapping
    public ResponseEntity add(@RequestBody FranchiseDto franchiseDto) {
        Franchise franchise = franchiseMapper.franchiseDtoToFranchise(franchiseDto);
        franchise = franchiseService.add(franchise);
        URI uri = URI.create("franchise/" + franchise.getId());
        return ResponseEntity.created(uri).build();
    }

    @Operation(summary = "Updates a franchise")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Franchise successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Franchise could not be found by provided ID",
                    content = @Content)
    })
    //Updates a franchise
    @PutMapping("{id}")
    public ResponseEntity update(@RequestBody FranchiseDto franchiseDto, @PathVariable int id) {
        if (franchiseDto.getId() != id) {
            return ResponseEntity.badRequest().build();
        }
        if (!franchiseService.franchiseExist(id)) {
            return ResponseEntity.notFound().build();
        }
        Franchise franchise = franchiseMapper.franchiseDtoToFranchise(franchiseDto);
        franchiseService.update(franchise);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Updates a franchises movies")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Franchise movies successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Franchise could not be found by provided ID",
                    content = @Content)
    })
    @PutMapping("{id}/movies")
    public ResponseEntity updateMovies(@RequestBody List<Integer> movies, @PathVariable int id) {
        if (!franchiseService.franchiseExist(id)) {
            return ResponseEntity.notFound().build();
        }
        franchiseService.updateMovies(movies, id);
        return ResponseEntity.noContent().build();
    }

    //Updates movies in a franchise
    @Operation(summary = "Deletes a franchise")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Franchise successfully deleted",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Franchise could not be found by provided ID",
                    content = @Content)
    })
    //Deletes a franchise
    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable int id) {
        if (!franchiseService.franchiseExist(id)) {
            return ResponseEntity.notFound().build();
        }
        franchiseService.deleteById(id);
        return ResponseEntity.noContent().build();
    }


}
