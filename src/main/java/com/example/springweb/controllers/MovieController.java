package com.example.springweb.controllers;

import com.example.springweb.mappers.MovieMapper;
import com.example.springweb.models.Movie;
import com.example.springweb.models.dtos.MovieDto;
import com.example.springweb.services.movies.MovieService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;
import java.util.Set;

@RestController
@RequestMapping(path = "api/v1/movies")
public class MovieController {
    private final MovieService movieService;
    private final MovieMapper movieMapper;

    public MovieController(MovieService movieService, MovieMapper movieMapper) {
        this.movieService = movieService;
        this.movieMapper = movieMapper;
    }

    @Operation(summary = "Get all movies")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = MovieDto.class)))})
    })
    //Gets all movies
    @GetMapping
    public ResponseEntity findAll() {
        Collection<MovieDto> movieDtos = movieMapper.movieToMovieDto(movieService.findAll());
        return ResponseEntity.ok(movieDtos);
    }

    @Operation(summary = "Get a movie by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieDto.class))}),
            @ApiResponse(responseCode = "404", content = {@Content(mediaType = "application/json")})
            //TODO Schema?
    })
    //Gets a movie by id
    @GetMapping("{id}")
    public ResponseEntity findById(@PathVariable int id) {
        MovieDto movieDto = movieMapper.movieToMovieDto(movieService.findById(id));
        return ResponseEntity.ok(movieDto);

    }

    @Operation(summary = "Adds a new movie")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description = "Movie successfully added",
                    content = @Content)
    })
    //Adds a movie to the database
    @PostMapping
    public ResponseEntity add(@RequestBody MovieDto movieDto) {

        Movie newMovie = movieMapper.movieDtoToMovie(movieDto);
        newMovie = movieService.add(newMovie);
        URI uri = URI.create("movie/" + newMovie.getId());
        return ResponseEntity.created(uri).build();
    }

    @Operation(summary = "Updates a movie")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Movie successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Movie could not be found by provided ID",
                    content = @Content)
    })
    //Updates a movie
    @PutMapping("{id}")
    public ResponseEntity update(@RequestBody MovieDto movieDto, @PathVariable int id) {
        if (movieDto.getId() != id) {
            return ResponseEntity.badRequest().build();
        }
        if (!movieService.movieExists(id)) {
            return ResponseEntity.notFound().build();
        }
        Movie movie = movieMapper.movieDtoToMovie(movieDto);
        movieService.update(movie);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Updates a movies characters")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Movie characters successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Movie could not be found by provided ID",
                    content = @Content)
    })
    //Updates which characters are in the movie
    @PutMapping("{id}/characters")
    public ResponseEntity updateCharacters(@RequestBody Set<Integer> characters, @PathVariable int id) {
        movieService.updateCharactersMovie(characters, id);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Deletes a movie")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Movie successfully deleted",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Movie could not be found by provided ID",
                    content = @Content)
    })
    //Deletes a movie
    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable int id) {
        if (!movieService.movieExists(id)) {
            return ResponseEntity.notFound().build();
        }
        movieService.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}
