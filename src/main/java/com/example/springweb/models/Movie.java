package com.example.springweb.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Getter
@Setter
public class Movie {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(length = 50, nullable = false)
    @NotBlank
    private String title;

    @Column(length = 50, nullable = false)
    private String genre;

    @Column(length = 50, nullable = false)
    private String releaseYear;

    @Column(length = 50, nullable = false)
    private String director;

    private String pictureUrl;

    private String trailerUrl;

    @ManyToOne
    private Franchise franchise;

    @ManyToMany(mappedBy = "movies")
    private Set<Character> characters;

    //TODO Remove when DTO is implemented
    @JsonGetter("characters")
    public List<Integer> jsonGetCharacters() {
        if (characters == null) {
            return null;
        }
        return characters.stream().map(x -> x.getId()).collect(Collectors.toList());
    }

}
