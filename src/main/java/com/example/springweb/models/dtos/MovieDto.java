package com.example.springweb.models.dtos;
import lombok.Data;
import java.util.Set;

@Data
public class MovieDto {
    private int id;
    private String title;
    private String genre;
    private String releaseYear;
    private String director;
    private String pictureUrl;
    private String trailerUrl;
    private int franchise;
    private Set<Integer> characters;

}
