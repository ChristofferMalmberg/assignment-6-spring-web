package com.example.springweb.mappers;


import com.example.springweb.models.Franchise;
import com.example.springweb.models.Movie;
import com.example.springweb.models.dtos.FranchiseDto;
import com.example.springweb.services.movies.MovieService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class FranchiseMapper {

    @Autowired
    MovieService movieService;

    //Converts a Franchise object to a Franchise DTO
    @Mapping(target = "movies", source = "movies", qualifiedByName = "moviesToIds")
    public abstract FranchiseDto franchiseToFranchiseDto(Franchise franchise);

    //Converts a list Franchise objects to a list of Franchise DTOs
    public abstract List<FranchiseDto> franchiseToFranchiseDto(List<Franchise> franchises);
    //Converts a Franchise DTO to a Franchise Object

    @Mapping(target = "movies", source = "movies", qualifiedByName = "IdsToMovies")
    public abstract Franchise franchiseDtoToFranchise(FranchiseDto franchiseDto);

    //Maps movies to movieIds
    @Named("moviesToIds")
    public Set<Integer> mapMovieToId(Set<Movie> movies) {
        if (movies == null) {
            return null;
        }
        return movies.stream().map(x -> x.getId()).collect(Collectors.toSet());
    }

    //Maps movie ids to movie objects
    @Named("IdsToMovies")
    public Set<Movie> mapIdsToMovie(Set<Integer> movieIds) {
        if (movieIds == null) {
            return null;
        }
        return movieIds.stream().map(x -> movieService.findById(x)).collect(Collectors.toSet());
    }
}
