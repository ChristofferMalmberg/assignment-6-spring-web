package com.example.springweb.mappers;

import com.example.springweb.models.Character;
import com.example.springweb.models.Movie;
import com.example.springweb.models.dtos.CharacterDto;
import com.example.springweb.services.movies.MovieService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class CharacterMapper {

    @Autowired
    MovieService movieService;

    //Converts a Character object to a Character DTO
    @Mapping(target = "movies", source = "movies", qualifiedByName = "moviesToIds")
    public abstract CharacterDto characterToCharacterDto(Character character);

    //Converts a list of Characters to a list of Character DTO's
    public abstract Collection<CharacterDto> characterToCharacterDto(Collection<Character> characters);

    //Converts a Character DTO to a Character object
    @Mapping(target = "movies", source = "movies", qualifiedByName = "movieIdsToMovies")
    public abstract Character characterDtoTOCharacter(CharacterDto characterDto);

    //Maps the movie ids to the movie objects
    @Named("movieIdsToMovies")
    Set<Movie> mapMovieIdsToMovie(Set<Integer> source) {
        if (source == null) {
            return null;
        }
        return source.stream().map(x -> movieService.findById(x)).collect(Collectors.toSet());
    }

    //Maps the movies of a character to the movie id's
    @Named("moviesToIds")
    Set<Integer> mapMoviesToIds(Set<Movie> source) {
        if (source == null) {
            return null;
        }
        return source.stream().map(x -> x.getId()).collect(Collectors.toSet());
    }
}
