package com.example.springweb.mappers;

import com.example.springweb.models.Character;
import com.example.springweb.models.Franchise;
import com.example.springweb.models.Movie;
import com.example.springweb.models.dtos.MovieDto;
import com.example.springweb.services.character.CharacterService;
import com.example.springweb.services.franchise.FranchiseService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class MovieMapper {

    @Autowired
    CharacterService characterService;
    @Autowired
    FranchiseService franchiseService;

    //Converts a Movie object to a Movie DTO
    @Mapping(target = "characters", source = "characters", qualifiedByName = "charactersToIds")
    @Mapping(target = "franchise", source = "franchise.id")
    public abstract MovieDto movieToMovieDto(Movie movie);

    //Converts a list Movie objects to a list if Movie DTOs
    public abstract Collection<MovieDto> movieToMovieDto(Collection<Movie> movies);

    //Converts a Movie DTO to a Movie object
    @Mapping(target = "characters", source = "characters", qualifiedByName = "characterIdsToCharacters")
    @Mapping(target = "franchise", source = "franchise", qualifiedByName = "franchiseIdToFranchise")
    public abstract Movie movieDtoToMovie(MovieDto movieDto);

    //Maps a franchise id to a franchise object
    @Named("franchiseIdToFranchise")
    Franchise mapFranchiseIdToFranchise(int id) {
        return franchiseService.findById(id);
    }

    //Maps character id's to character objects
    @Named("characterIdsToCharacters")
    Set<Character> mapCharacterIdsToCharacter(Set<Integer> source) {
        if (source == null) {
            return null;
        }
        return source.stream().map(x -> characterService.findById(x)).collect(Collectors.toSet());
    }

    //Maps character objects to ids
    @Named("charactersToIds")
    Set<Integer> mapCharactersToIds(Set<Character> source) {
        if (source == null) {
            return null;
        }
        return source.stream().map(x -> x.getId()).collect(Collectors.toSet());
    }
}

