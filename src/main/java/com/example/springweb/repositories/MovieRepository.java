package com.example.springweb.repositories;

import com.example.springweb.models.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Integer> {

    //Removes a character from the movie provided by movie id
    @Modifying
    @Query(value = "DELETE FROM movie_character WHERE movie_id = ?1", nativeQuery = true)
    @Transactional
    void removeCharacters(int characterId);

    //Adds the character to a movie by adding both ids in the linking table
    @Modifying
    @Query(value = "INSERT INTO movie_character (character_id, movie_id) VALUES (?1,?2)", nativeQuery = true)
    @Transactional
    void updateCharactersMovie(int characterId, int movieId);

    //Removes a movie from the linking table
    @Modifying
    @Query(value = "DELETE FROM movie_character WHERE movie_id = ?1", nativeQuery = true)
    @Transactional
    void deleteMovieLinkingTable(int id);
}
