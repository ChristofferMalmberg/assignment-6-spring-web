package com.example.springweb.repositories;

import com.example.springweb.models.Franchise;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface FranchiseRepository extends JpaRepository<Franchise, Integer> {

    //Updates movies in a franchise
    @Modifying
    @Query(value = "UPDATE movie SET franchise_id = ?2 WHERE id = ?1", nativeQuery = true)
    @Transactional
    void updateMovies(int movie, int franchiseId);
}
