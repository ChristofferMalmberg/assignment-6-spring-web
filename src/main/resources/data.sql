
INSERT INTO character (name, alias, gender, picture_url) VALUES ('Bruce Wayne', 'Batman', 'Male', null)
INSERT INTO character (name, alias, gender, picture_url) VALUES ('Peter Parker', 'Spider Man', 'Male', null)
INSERT INTO character (name, alias, gender, picture_url) VALUES ('Tony Stark', 'Iron Man', 'Male', null)
INSERT INTO character (name, alias, gender, picture_url) VALUES ('Selina Kyle', 'Cat Woman', 'Female', null)
INSERT INTO character (name, alias, gender, picture_url) VALUES ('Bruce Banner', 'The Hulk', 'Male', null)

INSERT INTO franchise (name, description) VALUES ('Marvel', 'Marvel franchise is the home for the most famous super heroes')
INSERT INTO franchise (name, description) VALUES ('DC', 'DC a franchise where people only truly care about Batman')

INSERT INTO movie (title, genre, release_year, director, picture_url, trailer_url, franchise_id) VALUES ('Avengers', 'Action, Super Heroes', '2012', 'Joss Whedon', null, null, 1)
INSERT INTO movie (title, genre, release_year, director, picture_url, trailer_url, franchise_id) VALUES ('The Dark Knight', 'Action, Super Heroes', '2008', 'Christopher Nolan', null, null, 2)
INSERT INTO movie (title, genre, release_year, director, picture_url, trailer_url, franchise_id) VALUES ('The Batman', 'Action, Super Heroes', '2022', 'Matt Reeves', null, null, 2)

INSERT INTO movie_character(movie_id, character_id) VALUES (1, 2)
INSERT INTO movie_character(movie_id, character_id) VALUES (1, 3)
INSERT INTO movie_character(movie_id, character_id) VALUES (1, 5)
INSERT INTO movie_character(movie_id, character_id) VALUES (2, 1)
INSERT INTO movie_character(movie_id, character_id) VALUES (3, 1)
INSERT INTO movie_character(movie_id, character_id) VALUES (3, 4)

