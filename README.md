# Spring web RESTApi Assignment 6
For this project a RESTApi is setup using spring boot, hibernate and postgres. Swagger(OpenAPI) is also included to provide documentation
for the project. With this repo you are able to setup the database in postgres with the help of hibernate and then make calls to the API which provides information about movies, characters and franchises. This project has also been deployed to heroku using docker and the documentation can be accessed <a href="https://assignment6-springweb.herokuapp.com/swagger-ui/index.html#/">here</a>.

## Requirements
The application can run locally with the following requirements.
- JDK 17
- Gradle
- Postgres
- Spring-web
- Swagger (OpenAPI)
## Install
- Clone project
- Open with Intellij or other preferred IDE
- Open application.properties and enter postgres DB credentials as below.
- Run project to setup DB and to be able to make calls to the API.
```
    spring.datasource.url= <Your DB URL>
    spring.datasource.username= <Your DB USERNAME>
    spring.datasource.password= <Your DB PASSWORD>
```


## Usage
If running locally the documentation should be available at: 
http://localhost:8080/swagger-ui/index.html#/ (if port unchanged)
Below is some example calls to the RESTApi when running locally.

```
        //Find all characters
        GET
        http://localhost:8080/api/v1/characters/

        //Finds the specific character which has an id of 1
        GET
        http://localhost:8080/api/v1/characters/1

        //Inserts a character into the database
        POST
        http://localhost:8080/api/v1/characters/
        example request body:
            {
            "name": "Spoder Man",
            "alias": "Peter porker",
            "gener": "Male",
            "pictureUrl": null,
            "movies": [
                1,
                3
            ]
            }

        //Deletes the specific character which has the id of 1
        DELETE
        http://localhost:8080/api/v1/characters/1

        //Updates the input character
        PUT
        http://localhost:8080/api/v1/characters/6
            example request body:
            {
            "id": "6"
            "name": "Slider Man",
            "alias": "Meter parker",
            "gener": "Male",
            "pictureUrl": null,
            "movies": [
                1,
                2
            ]
            }
```
To try out the deployed project the following links takes you to the documentation:
https://assignment6-springweb.herokuapp.com/swagger-ui/index.html#/
And calls can be made to:
https://assignment6-springweb.herokuapp.com/api/v1/{x} where x can be characters, movies or franchies

## Contributing
- @Maltin1234
- @ChristofferMalmberg 
